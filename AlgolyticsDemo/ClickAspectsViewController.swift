//
//  ClickAspectsViewController.swift
//  AlgolyticsDemo
//
//  Created by Mateusz Mirkowski on 01/07/2020.
//  Copyright © 2020 Mateusz Mirkowski. All rights reserved.
//

import UIKit
import AlgolyticsSDK

class ClickAspectsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        AlgolyticsSDK.shared.startGettingClickEvents(for: view)
        AlgolyticsSDK.shared.sendScreenName(name: "Click Aspects")
    }

    @IBAction func showAlert() {
        let alert = UIAlertController(title: "Alert", message: "This is alert.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            AlgolyticsSDK.shared.sendCustomEvent(identifier: "alert", value: "click-ok")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            AlgolyticsSDK.shared.sendCustomEvent(identifier: "alert", value: "click-cancel")
        }))

        self.present(alert, animated: true)
    }
}
