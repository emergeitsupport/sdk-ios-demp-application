//
//  ListViewController.swift
//  AlgolyticsDemo
//
//  Created by Mateusz Mirkowski on 01/07/2020.
//  Copyright © 2020 Mateusz Mirkowski. All rights reserved.
//

import UIKit
import AlgolyticsSDK

class ListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    let data = ["First", "Second", "Third", "Last"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self

        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        AlgolyticsSDK.shared.startGettingClickEvents(for: view)
        AlgolyticsSDK.shared.sendScreenName(name: "List Controller")
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ListTableViewCell
        cell?.titleLabel.text = data[indexPath.row]

        return cell ?? UITableViewCell.init(frame: .zero)
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        AlgolyticsSDK.shared.sendCustomEvent(identifier: "list-click", value: data[indexPath.row])
    }
}
