//
//  AppDelegate.swift
//  AlgolyticsDemo
//
//  Created by Mateusz Mirkowski on 30/06/2020.
//  Copyright © 2020 Mateusz Mirkowski. All rights reserved.
//

import UIKit
import AlgolyticsSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AlgolyticsSDK.shared.initWith(
            url: "https://demo.scoring.one/api/scenario/code/remote/score?name=iOSTracking&key=da1ae9dc-6909-4c6a-8f7f-3fa059a7aa83",
            apiKey: "api-key",
            apiPoolingTime: 10000,
            components: [
                .battery(poolingTime: 1000),
                .accelerometer(poolingTime: 1000),
                .calendar(poolingTime: 5000),
                .connectivity(poolingTime: 1000),
                .contact(poolingTime: 5000),
                .location(poolingTime: 1000),
                .photo(poolingTime: 5000),
                .wifi(poolingTime: 1000)
            ]
        )
        return true
    }

    // MARK: UISceneSession Lifecycle




}

