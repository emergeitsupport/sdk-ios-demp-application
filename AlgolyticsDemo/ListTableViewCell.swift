//
//  ListTableViewCell.swift
//  AlgolyticsDemo
//
//  Created by Mateusz Mirkowski on 01/07/2020.
//  Copyright © 2020 Mateusz Mirkowski. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
