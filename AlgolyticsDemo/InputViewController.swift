//
//  ViewController.swift
//  AlgolyticsDemo
//
//  Created by Mateusz Mirkowski on 30/06/2020.
//  Copyright © 2020 Mateusz Mirkowski. All rights reserved.
//

import UIKit
import AlgolyticsSDK

class InputViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var redTextView: UITextView!
    @IBOutlet weak var tealTextView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typ ically from a nib.
        searchBar.accessibilityTraits = UIAccessibilityTraits.searchField
        searchBar.isAccessibilityElement = true
        searchBar.accessibilityIdentifier = "koko"
        AlgolyticsSDK.shared.startGettingClickEvents(for: view)
        AlgolyticsSDK.shared.startGettingInputEvents(for: view)

        AlgolyticsSDK.shared.sendScreenName(name: "Input Screen")

        redTextView.delegate = self
        tealTextView.delegate = self

        let tapGestureReconizer = UITapGestureRecognizer(target: self, action: #selector(tap(sender:)))
        view.addGestureRecognizer(tapGestureReconizer)
    }

    @objc func tap(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @objc func cancelDatePicker() {
       self.view.endEditing(true)
     }
}

extension InputViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        AlgolyticsSDK.shared.sendTextViewBeginEditingEvent(textView)
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        AlgolyticsSDK.shared.sendTextViewEndEditingEvent(textView)
    }

    func textViewDidChange(_ textView: UITextView) {
        AlgolyticsSDK.shared.sendTextViewEvent(textView)
    }
}


